package kjd.iot_researcher;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import io.github.kexanie.library.MathView;

    // 공식 입력 및 변수 값 설정을 위한 Activity
public class FormulaActivity extends AppCompatActivity implements View.OnClickListener {
    MathView mathView;
    ImageView plus, minus, mul, divide, root, square;
    TextView var1, var2, var3;
    String save_formula = "\\(";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formula);

        mathView = (MathView) findViewById(R.id.math_view);
        plus = (ImageView) findViewById(R.id.plus);
        minus = (ImageView) findViewById(R.id.minus);
        mul = (ImageView) findViewById(R.id.multiple);
        divide = (ImageView) findViewById(R.id.divide);
        root = (ImageView) findViewById(R.id.root);
        square = (ImageView) findViewById(R.id.square);
        var1 = (TextView) findViewById(R.id.var1);
        var2 = (TextView) findViewById(R.id.var2);
        var3 = (TextView) findViewById(R.id.var3);

        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        mul.setOnClickListener(this);
        divide.setOnClickListener(this);
        root.setOnClickListener(this);
        square.setOnClickListener(this);
        var1.setOnClickListener(this);
        var2.setOnClickListener(this);
        var3.setOnClickListener(this);
    }

        // 각 아이템이 터치 되었을 경우 판단
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.plus:
                if(save_formula.equals("\\(")){
                    Toast.makeText(FormulaActivity.this, "변수를 사용해 주세요.", Toast.LENGTH_SHORT).show();
                }
                else{
                    save_formula = save_formula+" + ";
                    mathView.setText(save_formula+"\\)");
                }
                break;
            case R.id.minus:
                if(save_formula.equals("\\(")){
                    Toast.makeText(FormulaActivity.this, "변수를 사용해 주세요.", Toast.LENGTH_SHORT).show();
                }
                else{
                    save_formula = save_formula+" - ";
                    mathView.setText(save_formula+"\\)");
                }
                break;
            case R.id.multiple:
                if(save_formula.equals("\\(")){
                    Toast.makeText(FormulaActivity.this, "변수를 사용해 주세요.", Toast.LENGTH_SHORT).show();
                }
                else{
                    save_formula = save_formula+" * ";
                    mathView.setText(save_formula+"\\)");
                }
                break;
            case R.id.divide:
                if(save_formula.equals("\\(")){
                    Toast.makeText(FormulaActivity.this, "변수를 사용해 주세요.", Toast.LENGTH_SHORT).show();
                }
                else{
                    save_formula = save_formula+" \\over ";
                    mathView.setText(save_formula+"\\)");
                }
                break;
            case R.id.root:
                if(save_formula.equals("\\(")){
                    save_formula = save_formula+" \\sqrt ";
                    mathView.setText(save_formula+"\\)");
                }
                else{
                    save_formula = save_formula+" \\sqrt ";
                    mathView.setText(save_formula+"\\)");
                }
                break;
            case R.id.square:
                if(save_formula.equals("\\(")){
                    Toast.makeText(FormulaActivity.this, "변수를 사용해 주세요.", Toast.LENGTH_SHORT).show();
                }
                else{
                    save_formula = save_formula+"^";
                    mathView.setText(save_formula+"\\)");
                }
                break;
            case R.id.var1:
                save_formula = save_formula + "x";
                mathView.setText(save_formula+"\\)");
                break;
            case R.id.var2:
                save_formula = save_formula + "y";
                mathView.setText(save_formula+"\\)");
                break;
            case R.id.var3:
                save_formula = save_formula + "z";
                mathView.setText(save_formula+"\\)");
                break;
        }
    }
}
