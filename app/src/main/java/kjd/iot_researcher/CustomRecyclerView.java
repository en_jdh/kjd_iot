package kjd.iot_researcher;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.AttributeSet;

import kjd.item.ItemAdapter;
import kjd.item.ItemTouchHelperCallback;
import kjd.item.MyItemAnimator;

// 아이템들의 리스트를 만들기 위해 RecyclerView를 사용 -> 기존 RecyclerView를 변형 사용을 위해 Custom으로 만듬
public class CustomRecyclerView extends RecyclerView {

    // RecyclerView의 Animation 등록
    public CustomRecyclerView(Context context) {
        super(context);
        setItemAnimator(new MyItemAnimator());
    }

    public CustomRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setItemAnimator(new MyItemAnimator());
    }

    public CustomRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setItemAnimator(new MyItemAnimator());
    }

    // RecyclerView의 Adapter 등록
    @Override
    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);
        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelperCallback((ItemAdapter)adapter));
        helper.attachToRecyclerView(this);
    }

}