package kjd.iot_researcher;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import kjd.main_acitivity.ProjectActivity;

// New Project시 나오는 첫 번째 Fragment(Free Experiment)
public class ChooseFragmentFirst extends Fragment implements View.OnClickListener {
    View view;
    Activity root;
    ImageView imageView;

    public ChooseFragmentFirst() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.choose_fragment_one, container, false);
        root = getActivity();

        imageView = (ImageView) view.findViewById(R.id.free_experiment);
        imageView.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.free_experiment:
                root.finish();
                startActivity(new Intent(root,ProjectActivity.class));
                break;
        }
    }
}


