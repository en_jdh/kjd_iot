package kjd.iot_researcher;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;

// 해당 Project의 DB값을 이용하여 Matlab을 통한 분석
// 네트워크 작업이므로 Thread 실행
public class execute_matlab {
    String num = value_save.list_num+"";
    execute_matlab(){
        getData("http://kjdiot.iptime.org/plot/start.php?num="+num);
    }


    public void getData(final String url){

        class GetDataJSON extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                OkHttpClient client = new OkHttpClient();

                //request
                Request request1 = new Request.Builder()
                        .url("http://kjdiot.iptime.org/plot/distance.php")
                        .build();
                Request request2 = new Request.Builder()
                        .url("http://kjdiot.iptime.org/plot/temp.php")
                        .build();

                Request request3 = new Request.Builder()
                        .url(url)
                        .build();

                try {
                    client.newCall(request1).execute();
                    client.newCall(request2).execute();
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException i) {
                        i.printStackTrace();
                    }
                    client.newCall(request3).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                return null;
            }

        }
        GetDataJSON g = new GetDataJSON();
        g.execute(url);
    }

}