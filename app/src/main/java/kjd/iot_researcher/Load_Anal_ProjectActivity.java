package kjd.iot_researcher;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import kjd.item.load_listviewAdapter;

// Analyze를 위한 Load_Project
public class Load_Anal_ProjectActivity extends AppCompatActivity {
    ListView listView;
    ProgressBar progressBar;
    load_listviewAdapter adapter;
    ArrayList<String> name;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_project);
        tostart();
        adapter = new load_listviewAdapter();
        name = new ArrayList<>();
        listView.setAdapter(adapter);
        final SharedPreferences count_data = getSharedPreferences("count", MODE_PRIVATE);
        if(count_data.getInt("count",-1)==0){
            count = -1;
        }
        else{
            count = count_data.getInt("count", -1);
        }


        // listview의 아이템을 그냥 클릭 할 경우(한 번 터치)
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int e, long l) {
                progressBar.setVisibility(View.VISIBLE);
                value_save.list_num = count -e;
                execute_matlab executeMatlab = new execute_matlab();
                Toast.makeText(Load_Anal_ProjectActivity.this, "분석중.......", Toast.LENGTH_LONG).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.INVISIBLE);
                        startActivity(new Intent(Load_Anal_ProjectActivity.this, Analyze_Project.class).putExtra("click_num", e));
                    }
                },15000);
            }
        });
    }

    /// ProjectActivity에서 OutputStream을 통해 파일로 저장된 Object를 가지고 온다 ///
    public void get_data() {
        Log.d("111111",count+"");
        for (int i = count; i > 0 ; i--) {
            Log.d("111111","listview 입력 파일 : "+i);
            try {
                ObjectInputStream ois = new ObjectInputStream(
                        new BufferedInputStream(
                                new FileInputStream(
                                        getFilesDir().getAbsolutePath() + "/name_data"+i+".ser"
                                )
                        )
                );
                name = (ArrayList<String>) ois.readObject();
                ois.close();
                adapter.addItem(name.get(0), name.get(1));

            } catch (Exception e) {
                Toast.makeText(Load_Anal_ProjectActivity.this, "오류", Toast.LENGTH_SHORT).show();
            }
            adapter.notifyDataSetChanged();
        }

    }

    /// 수정 후 돌아왔을 때, 바로 갱신 되게 onResume()에서 데이터를 처리해 주었다 ///
    @Override
    protected void onResume() {
        super.onResume();
        adapter.listviewItemList.clear();
        get_data();
    }

    /// view들의 초기화와 리스너 등록 ///
    public void tostart(){
        listView = (ListView) findViewById(R.id.listView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

}
