package kjd.iot_researcher;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

// 입력 받은 값들을 서버로 보내기위한 class (서버에서 아두이노를 실행시킨다.)
// 네트워크 작업이므로 Thread 실행
public class get_plot {
    String myJSON;
    private static final String TAG_RESULTS="result";
    JSONArray favorite_json = null;
    String num = value_save.list_num+"";
    get_plot(){
        getData("http://kjdiot.iptime.org/dong/get_plot.php?num="+num);
    }

    protected void showList(){
        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            favorite_json = jsonObj.getJSONArray(TAG_RESULTS);

            for(int i=0;i<favorite_json.length();i++){
                JSONObject c = favorite_json.getJSONObject(i);
                value_save.plot_num = c.getInt("num");
                value_save.plot_count = c.getInt("count");
                value_save.plot_one = c.getString("one");
                value_save.plot_two = c.getString("two");
                value_save.plot_three = c.getString("three");
                value_save.plot_four = c.getString("four");
                value_save.plot_five = c.getString("five");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getData(final String url){

        class GetDataJSON extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                OkHttpClient client = new OkHttpClient();

                //request
                Request request = new Request.Builder()
                        .url(url)
                        .build();

                client.newCall(request).enqueue(callback);


                return null;
            }

        }
        GetDataJSON g = new GetDataJSON();
        g.execute(url);
    }

    private Callback callback = new Callback() {
        @Override
        public void onFailure(Call call, IOException e) {

        }

        @Override
        public void onResponse(Call call, Response response) throws IOException {
            myJSON = response.body().string();
            showList();
        }
    };
}