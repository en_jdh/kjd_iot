package kjd.iot_researcher;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import kjd.tool.Option_Activity;

// New Project시 나오는 두 번째 Fragment(MOSFET)
public class ChooseFragmentSecond extends Fragment implements View.OnClickListener {
    View view;
    Activity root;
    ImageView imageView;

    public ChooseFragmentSecond() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.choose_fragment_two, container, false);
        root = getActivity();

        imageView = (ImageView) view.findViewById(R.id.mosfet);
        imageView.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mosfet:
                startActivity(new Intent(root,Option_Activity.class));
                break;
        }
    }
}


