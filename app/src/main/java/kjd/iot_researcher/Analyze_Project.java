package kjd.iot_researcher;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import kjd.adapter.Anal_Adapter;
import me.relex.circleindicator.CircleIndicator;

// Plot을 보여주기 위한 Activity
public class Analyze_Project extends AppCompatActivity {
    ViewPager plot_pager;
    CircleIndicator indicator;
    Anal_Adapter anal_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analyze);
        get_plot getPlot = new get_plot();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                plot_pager = (ViewPager) findViewById(R.id.anal_viewpager);
                indicator = (CircleIndicator) findViewById(R.id.indicator);
                anal_adapter = new Anal_Adapter(getLayoutInflater());
                plot_pager.setAdapter(anal_adapter);
                indicator.setViewPager(plot_pager);
                anal_adapter.notifyDataSetChanged();
            }
        },2000);

    }
}
