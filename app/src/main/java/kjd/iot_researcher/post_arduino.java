package kjd.iot_researcher;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

// 입력 받은 값들을 서버로 보내기위한 class (서버에서 아두이노를 실행시킨다.)
// 네트워크 작업이므로 Thread 실행
public class post_arduino {
    public void putData(final String url){

        // AsyncTask를 통해 Thread 처리
        class GetDataJSON extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                // OkHttp를 통한 통신
                OkHttpClient client = new OkHttpClient();
                RequestBody body = new FormBody.Builder()
                        .build();

                //request
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();

                try{
                    client.newCall(request).execute();
                }catch (IOException e) {
                    e.printStackTrace();
                }


                return null;
            }

        }
        GetDataJSON g = new GetDataJSON();
        g.execute(url);
    }
}
