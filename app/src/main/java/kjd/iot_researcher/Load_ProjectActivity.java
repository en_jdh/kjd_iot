package kjd.iot_researcher;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import kjd.item.load_listviewAdapter;
import kjd.main_acitivity.Adjust_ProjectActivity;

// Load_Project에서 사용한 Activity
public class Load_ProjectActivity extends AppCompatActivity {
    ListView listView;
    load_listviewAdapter adapter;
    ArrayList<String> name;
    int count;
    WebView delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_project);
        tostart();
        adapter = new load_listviewAdapter();
        name = new ArrayList<>();
        listView.setAdapter(adapter);
        final SharedPreferences count_data = getSharedPreferences("count", MODE_PRIVATE);
        if(count_data.getInt("count",-1)==0){
            count = -1;
        }
        else{
            count = count_data.getInt("count", -1);
        }

        // listview의 아이템을 그냥 클릭 할 경우(한 번 터치)
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int e, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Load_ProjectActivity.this);
                builder.setTitle("알림")
                        .setMessage("어떤 기능을 수행하시겠습니까?")
                        .setCancelable(false)
                        .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .setNeutralButton("공식 입력", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(Load_ProjectActivity.this,FormulaActivity.class));
                            }
                        })
                        .setPositiveButton("수정 및 실행", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(Load_ProjectActivity.this,Adjust_ProjectActivity.class).putExtra("click_num",e));
                            }
                        });
                Dialog dialog = builder.create();
                dialog.show();
            }
        });

        // listview의 아이템을 꾹 누르고 있는 경우 이벤트(삭제를 위해)
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> adapterView, View view, final int e, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Load_ProjectActivity.this);
                builder.setTitle("삭제")
                        .setMessage("선택하신 Project를 삭제 하시겠습니까?")
                        .setCancelable(false)
                        .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .setPositiveButton("삭제", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                int get_num = count-e;
                                Log.d("111111","삭제 파일 : "+get_num) ;
                                delete.loadUrl("http://kjdiot.iptime.org/dong/dong_put_ex_change.php?num="+get_num);
                                File file = new File(getFilesDir().getAbsolutePath() + "/name_data"+get_num+".ser");
                                File inner_file = new File(getFilesDir().getAbsolutePath() + "/inner_data"+get_num+".ser");
                                file.delete();
                                inner_file.delete();
                                adapter.remove(e); // listview에서 삭제 notify도 안에 들어있음
                                // 삭제를 통해 파일이 삭제되었을 시 삭제된것보다 번호가 높은 파일들의 번호를 1씩 낮춤
                                // 이작업을 통해 파일의 이름 번호를 항상 맞출 수 있음(Count 수와 같게)
                                for(int a = get_num+1; a <= count ; a++){
                                    file = new File(getFilesDir().getAbsolutePath()+ "/name_data"+a+".ser");
                                    inner_file = new File(getFilesDir().getAbsolutePath()+ "/inner_data"+a+".ser");
                                    file.renameTo(new File(getFilesDir().getAbsolutePath()+ "/name_data"+(a-1)+".ser"));
                                    inner_file.renameTo(new File(getFilesDir().getAbsolutePath()+ "/inner_data"+(a-1)+".ser"));
                                    Log.d("111111",a+"  에서  "+(a-1)+"로");
                                }
                                SharedPreferences.Editor editor = count_data.edit();
                                editor.putInt("count",count_data.getInt("count",0)-1);
                                editor.commit();
                                count = count_data.getInt("count",0);
                                Toast.makeText(Load_ProjectActivity.this, "삭제되었습니다.", Toast.LENGTH_SHORT).show();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
        });
    }

    /// ProjectActivity에서 OutputStream을 통해 파일로 저장된 Object를 가지고 온다 ///
    public void get_data() {
        Log.d("111111",count+"");
        for (int i = count; i > 0 ; i--) {
            Log.d("111111","listview 입력 파일 : "+i);
            try {
                ObjectInputStream ois = new ObjectInputStream(
                        new BufferedInputStream(
                                new FileInputStream(
                                        getFilesDir().getAbsolutePath() + "/name_data"+i+".ser"
                                )
                        )
                );
                name = (ArrayList<String>) ois.readObject();
                ois.close();
                adapter.addItem(name.get(0), name.get(1));

            } catch (Exception e) {
                Toast.makeText(Load_ProjectActivity.this, "오류", Toast.LENGTH_SHORT).show();
            }
            adapter.notifyDataSetChanged();
        }

    }

    /// 수정 후 돌아왔을 때, 바로 갱신 되게 onResume()에서 데이터를 처리해 주었다 ///
    @Override
    protected void onResume() {
        super.onResume();
        adapter.listviewItemList.clear();
        get_data();
    }

    /// view들의 초기화와 리스너 등록 ///
    public void tostart(){
        listView = (ListView) findViewById(R.id.listView);
        delete = (WebView) findViewById(R.id.delete);
    }
}
