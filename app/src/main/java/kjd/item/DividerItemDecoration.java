package kjd.item;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import kjd.iot_researcher.R;

public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    private Drawable parentDivider;
    private Drawable subDivider;


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DividerItemDecoration(Context context) {
        parentDivider = context.getDrawable(R.drawable.parent_divider);
        subDivider = context.getDrawable(R.drawable.sub_divider);
    }

    /// canvas에 적합한 decoration을 draw ///
    /// item view가 그려지기전에 decoration이 먼저 그려지며, view밑에 나타난다. ///
    @Override
    public void onDraw(Canvas c, RecyclerView recyclerView, RecyclerView.State state) {

        super.onDraw(c, recyclerView, state);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        RecyclerView.ViewHolder viewHolder = (parent.getChildViewHolder(view));

        if(viewHolder instanceof ItemAdapter.ParentItemVH){
            outRect.set(0, 1, 0, 1);
        }else{
            outRect.set(20, 1, 20, 1);
        }

    }

}