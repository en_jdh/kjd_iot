package kjd.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kjd.iot_researcher.R;


// Load_ProjectActivity(저장 목록)에서 사용되어지는 listview의 Adapter
public class load_listviewAdapter extends BaseAdapter {
    protected ArrayList<load_listviewItem> listviewItemList = new ArrayList<>();

    @Override
    public int getCount() {
        return listviewItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return listviewItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context context = parent.getContext();

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.load_listview,parent,false);
        }

        TextView name = (TextView) convertView.findViewById(R.id.project_name);
        TextView date = (TextView) convertView.findViewById(R.id.date);
        load_listviewItem listviewItem = listviewItemList.get(position);
        name.setText(listviewItem.getName());
        date.setText(listviewItem.getDate());

        return convertView;
    }

// ListView에 item을 추가하기 Load_ProjectActivity에서 호출
public void addItem(String name, String date){
    load_listviewItem item = new load_listviewItem();
    item.setName(name);
    item.setDate(date);
    listviewItemList.add(item);
}
    // remove()를 통해 리스트에서 아이템이 사라지고 notify를 통해 바뀐것으 알려 새로고침
    public void remove(int i){
        listviewItemList.remove(i);
        notifyDataSetChanged();
    }
}
