package kjd.item;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

public class ItemTouchHelperCallback extends ItemTouchHelper.Callback {

    public final String TAG = "[Simple][ItemTouchHelperCallback]";
    private final int PARENT_ITEM_VIEW = 0;
    private final int CHILD_ITEM_VIEW = 1;
    ItemTouchHelperListener listener;

    public ItemTouchHelperCallback(ItemTouchHelperListener listener){
        this.listener = listener;
    }

    // 각 view에서 어떤 user action이 가능한지 정의( 자식뷰들은 action을 받지 않음 )
    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        if(viewHolder.getItemViewType() == PARENT_ITEM_VIEW) {
            // Drag(꾹눌러 이동) -> 리스트 변경을 위해
            int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
            // Swipe(옆으로 제스처) -> 삭제를 위해
            int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
            return makeMovementFlags(dragFlags, swipeFlags);
        }
        return makeMovementFlags(0, 0);
    }


    // 사용자가 item을 drag할 때, ItemTouchHelper가 onMove()를 실행
    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder source, RecyclerView.ViewHolder target) {
            listener.onItemMove(source.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    // view가 swipe될 때, ItemTouchHelper는 뷰가 사라질때까지 animate한 후, onSwiped()를 실행(해당 view 삭제)
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            listener.onItemRemove(viewHolder.getAdapterPosition());
    }
}