package kjd.item;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import kjd.iot_researcher.R;

// 아이템과 RecyclerView를 연결시켜주는 Adapter
public class ItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements ItemTouchHelperListener {

    private final String TAG = "[Simple][NewItemAdaptr]";
    private final int PARENT_ITEM_VIEW = 0;
    private final int CHILD_ITEM_VIEW = 1;

    private ArrayList<Item> items = new ArrayList<>();
    public ArrayList<Item> visibleItems = new ArrayList<>();
    protected ArrayList<String> item_list = new ArrayList<>();
    protected ArrayList<String> child_list = new ArrayList<>();
    String module;

    // ProjectActivity와 Adjust_ProjectActivity에서 drop action을 통해 add_item() 을 실행하여 각 값에 맞는 아이템을 recyclerview에 등록시켜줌
    public void add_item(String add_module, String data){
        if(add_module.equals("push")){
            Log.d("111111","push");
            Item item = new ParentItem("PUSH",PARENT_ITEM_VIEW);
            items.add(item);
            visibleItems.add(item);
            item_list.add("push");
            notifyDataSetChanged();
        }
        else if(add_module.equals("spin")){
            Log.d("111111","spin");
            Item item = new ParentItem("SPIN",PARENT_ITEM_VIEW);
            Item item1 = new ChildItem("각도 : "+ data,CHILD_ITEM_VIEW);
            items.add(item);
            items.add(item1);
            visibleItems.add(item);
            visibleItems.add(item1);
            item_list.add("spin");
            item_list.add("spin_value");
            notifyDataSetChanged();
        }
        else if(add_module.equals("detect")){
            Log.d("111111","detect");
            Item item = new ParentItem("DETECT",PARENT_ITEM_VIEW);
            Item item1 = new ChildItem("검출 온도: "+ data,CHILD_ITEM_VIEW);
            items.add(item);
            items.add(item1);
            visibleItems.add(item);
            visibleItems.add(item1);
            item_list.add("detect");
            item_list.add("detect_value");
            notifyDataSetChanged();
        }
        else if(add_module.equals("temp")){
            Log.d("111111","temp");
            Item item = new ParentItem("TEMPERATURE",PARENT_ITEM_VIEW);
            items.add(item);
            visibleItems.add(item);
            item_list.add("temp");
            notifyDataSetChanged();
        }
        else if(add_module.equals("text")){
            Log.d("111111","text");
            module = "text";
            Item item = new ParentItem("TEXT",PARENT_ITEM_VIEW);
            Item item1 = new ChildItem(data,CHILD_ITEM_VIEW);
            items.add(item);
            items.add(item1);
            visibleItems.add(item);
            visibleItems.add(item1);
            item_list.add("text");
            item_list.add("text_value = "+data);
            notifyDataSetChanged();
        }
        else if(add_module.equals("delay")){
            Item item = new ParentItem("DELAY",PARENT_ITEM_VIEW);
            Item item1 = new ChildItem("지연 시간 : "+ data,CHILD_ITEM_VIEW);
            items.add(item);
            items.add(item1);
            visibleItems.add(item);
            visibleItems.add(item1);
            item_list.add("delay");
            item_list.add("delay_value = "+data);
            notifyDataSetChanged();
        }
    }


    // item 개수 return
    @Override
    public int getItemCount() {
        return visibleItems.size();
    }

    // 해당 Postion에 있는 아이템이 자식인지 부모인지 return
    @Override
    public int getItemViewType(int position) {
        return visibleItems.get(position).viewType;
    }

    // ViewHolder를 통한 RecyclerView에 아이탬을 홀드 시킴
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;

        switch(viewType){
            case PARENT_ITEM_VIEW:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
                viewHolder = new ParentItemVH(view);
                break;
            case CHILD_ITEM_VIEW:
                View subview = LayoutInflater.from(parent.getContext()).inflate(R.layout.subitem, parent, false);
                viewHolder = new ChildItemVH(subview);
                break;
        }

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ParentItemVH){
            ParentItemVH parentItemVH = (ParentItemVH)holder;
            parentItemVH.name.setText(visibleItems.get(position).name);
            parentItemVH.arrow.setTag(holder);

            // Parent Item의 화살표를 눌렀을 때 position에 따라서 Child Item이 접히고 펼쳐지게
            parentItemVH.arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int holderPosition = ((ParentItemVH)v.getTag()).getAdapterPosition();
                    if(((ParentItem)visibleItems.get(holderPosition)).visibilityOfChildItems){
                        collapseChildItems(holderPosition);
                    }else{
                        expandChildItems(holderPosition);
                    }
                }
            });

            // ParentItem을 움직일때 Child Item을 접고 움직이게
            parentItemVH.itemView.setTag(holder);
            if(parentItemVH.getItemViewType() == PARENT_ITEM_VIEW) {
                parentItemVH.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        int holderPosition = ((ParentItemVH)v.getTag()).getAdapterPosition();
                        if(((ParentItem)visibleItems.get(holderPosition)).visibilityOfChildItems){
                            collapseChildItems(holderPosition);
                        }
                        return true;
                    }
                });
            }

        }else if(holder instanceof ChildItemVH){
            ((ChildItemVH)holder).name.setText(visibleItems.get(position).name);
        }
    }

    private void collapseChildItems(int position){
        ParentItem parentItem = (ParentItem)visibleItems.get(position);
        parentItem.visibilityOfChildItems = false;

        int subItemSize = getVisibleChildItemSize(position);
        for(int i = 0; i < subItemSize; i++){
            parentItem.unvisibleChildItems.add((ChildItem) visibleItems.get(position + 1));
//            child_list.add(item_list.get(position+1));
            visibleItems.remove(position + 1);
//            item_list.remove(position+1);
        }

        notifyItemRangeRemoved(position + 1, subItemSize);
    }

    private int getVisibleChildItemSize(int parentPosition){
        int count = 0;
        parentPosition++;
        while(true){
            if(parentPosition == visibleItems.size() || visibleItems.get(parentPosition).viewType == PARENT_ITEM_VIEW){
                break;
            }else{
                parentPosition++;
                count++;
            }
        }
        return count;
    }

    public void expandChildItems(int position){

        ParentItem parentItem = (ParentItem)visibleItems.get(position);
        parentItem.visibilityOfChildItems = true;
        int childSize = parentItem.unvisibleChildItems.size();

        for(int i = childSize - 1; i >= 0; i--){
            visibleItems.add(position + 1, parentItem.unvisibleChildItems.get(i));
//            item_list.add(position+1,child_list.get(i));
        }
        parentItem.unvisibleChildItems.clear();
        child_list.clear();

        notifyItemRangeInserted(position + 1, childSize);
    }


    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if(fromPosition < 0 || fromPosition >= visibleItems.size() || toPosition < 0 || toPosition >= visibleItems.size()){
            return false;
        }

        Item fromItem = visibleItems.get(fromPosition);
//        String from = item_list.get(fromPosition);

        if(visibleItems.get(fromPosition).viewType == CHILD_ITEM_VIEW){
            if(fromPosition <= 0 || toPosition <= 0){
                return false;
            }

            visibleItems.remove(fromPosition);
//            item_list.remove(fromPosition);
            visibleItems.add(toPosition, fromItem);
//            item_list.add(toPosition,from);


            notifyItemMoved(fromPosition, toPosition);

        }else{
            if(visibleItems.get(fromPosition).viewType == visibleItems.get(toPosition).viewType) {
                if(fromPosition > toPosition){
                    visibleItems.remove(fromPosition);
//                    item_list.remove(fromPosition);
                    visibleItems.add(toPosition, fromItem);
//                    item_list.add(toPosition,from);
                    notifyItemMoved(fromPosition, toPosition);
                }else{
                    int toParentPosition = getParentPosition(toPosition);
                    int toLastchildSize = getVisibleChildItemSize(toParentPosition);

                    if(toLastchildSize == 0){

                        visibleItems.remove(fromPosition);
//                        item_list.remove(fromPosition);
                        visibleItems.add(toPosition, fromItem);
//                        item_list.add(toPosition,from);

                        notifyItemMoved(fromPosition, toPosition);
                    }

                }
            }else{
                if(fromPosition < toPosition){
                    int toParentPosition = getParentPosition(toPosition);
                    int toLastchildPosition = getVisibleChildItemSize(toParentPosition) + toParentPosition;

                    if(toLastchildPosition == toPosition) {

                        visibleItems.remove(fromPosition);
//                        item_list.remove(fromPosition);
                        visibleItems.add(toPosition, fromItem);
//                        item_list.add(toPosition,from);

                        notifyItemMoved(fromPosition, toPosition);
                    }
                }
            }
        }

        return true;
    }

    @Override
    public void onItemRemove(int position) {
        switch(visibleItems.get(position).viewType){
            case PARENT_ITEM_VIEW:
                int childItemSize = getVisibleChildItemSize(position);

                for(int i = 0; i <= childItemSize; i++){
                    visibleItems.remove(position);
//                    item_list.remove(position);
                }
                notifyItemRangeRemoved(position, childItemSize + 1);

                break;
            case CHILD_ITEM_VIEW:
                visibleItems.remove(position);
//                item_list.remove(position);
                notifyItemRemoved(position);
                break;
        }
    }

    private int getParentPosition(int position){
        while(true){
            if(visibleItems.get(position).viewType == PARENT_ITEM_VIEW){
                break;
            }else{
                position--;
            }
        }
        return position;
    }

    public class ParentItemVH extends RecyclerView.ViewHolder {

        TextView name;
        ImageButton arrow;

        public ParentItemVH(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.item_name);
            arrow = (ImageButton)itemView.findViewById(R.id.item_arrow);
        }
    }

    public class ChildItemVH extends RecyclerView.ViewHolder {

        TextView name;

        public ChildItemVH(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.subitem_name);
        }
    }
}