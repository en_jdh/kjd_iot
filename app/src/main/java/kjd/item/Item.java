package kjd.item;

import java.io.Serializable;

// 아이템 등록을 위한 class
public abstract class Item implements Serializable {
    public int getViewType() {
        return viewType;
    }
    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String name;
    public int viewType = 0;

}