package kjd.item;

import java.io.Serializable;
import java.util.ArrayList;

// 부모 아이템을 관리하기 위한 아이템
public class ParentItem extends Item implements Serializable {

    public boolean visibilityOfChildItems = true;
    public ArrayList<ChildItem> unvisibleChildItems = new ArrayList<>();

    // 부모 아이템의 이름과 부모 아이템이란 Type을 정의
    public ParentItem(String name, int viewType){
        this.name = name;
        this.viewType = viewType;
    }


}