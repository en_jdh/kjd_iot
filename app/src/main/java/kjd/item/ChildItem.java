package kjd.item;

import java.io.Serializable;

// 자식 아이템을 관리하기 위한 아이템
public class ChildItem extends Item implements Serializable {

    // 자식 아이템의 이름과 자식 아이템이란 Type을 정의
    public ChildItem(String name, int viewType){


        this.name = name;
        this.viewType = viewType;
    }
}