package kjd.item;

//Listner를 통해 쉽게 ItemTouchHelperCallback에 정의 되어있는 2개의 함수를 사용
public interface ItemTouchHelperListener {
    boolean onItemMove(int fromPosition, int toPosition);
    void onItemRemove(int position);
}