package kjd.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import kjd.iot_researcher.R;
import kjd.iot_researcher.value_save;
import uk.co.senab.photoview.PhotoView;

public class Anal_Adapter extends PagerAdapter {
    LayoutInflater inflater;

    private String[] GalImages = new String[]{
            value_save.plot_one,
            value_save.plot_two,
            value_save.plot_three,
            value_save.plot_four,
            value_save.plot_five
    };
    public Anal_Adapter(LayoutInflater inflater){
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return value_save.plot_count;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==object;
    }



    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = null;

        view = inflater.inflate(R.layout.anal_plot,null);
        PhotoView plot = (PhotoView) view.findViewById(R.id.anal_plot);
        Glide.with(view.getContext()).load(GalImages[position])
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(plot);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
    }
}
