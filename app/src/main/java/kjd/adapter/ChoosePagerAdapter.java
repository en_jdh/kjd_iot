package kjd.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import kjd.iot_researcher.ChooseFragmentFirst;
import kjd.iot_researcher.ChooseFragmentSecond;
import kjd.iot_researcher.ChooseFragmentThird;

/**
 * Created by j-pc on 2016-12-27.
 */
public class ChoosePagerAdapter extends FragmentPagerAdapter {

    int tabCount;

    public ChoosePagerAdapter(FragmentManager fm, int numberOfTabs){
        super(fm);
        this.tabCount = numberOfTabs;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                ChooseFragmentFirst tab1 = new ChooseFragmentFirst();
                return tab1;
            case 1:
               ChooseFragmentSecond tab2 = new ChooseFragmentSecond();
                return tab2;
            case 2:
                ChooseFragmentThird tab3 = new ChooseFragmentThird();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
