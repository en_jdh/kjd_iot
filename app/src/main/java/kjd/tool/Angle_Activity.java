package kjd.tool;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import kjd.main_acitivity.Adjust_ProjectActivity;
import kjd.main_acitivity.ProjectActivity;
import kjd.iot_researcher.R;
import kjd.iot_researcher.value_save;


// Angle 아이템을 선택했을 경우 각도를 입력하기 위해 팝업으로 뜨는 Activity
public class Angle_Activity extends Activity{
    static DiscreteSeekBar angle;
    TextView module_one;
    TextView module_two;
    @Override
    public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            layoutParams.dimAmount = 0.7f;
            getWindow().setAttributes(layoutParams);
            setContentView(R.layout.angle_layout);

            this.setFinishOnTouchOutside(false);
            angle = (DiscreteSeekBar) findViewById(R.id.angle);
            module_one = (TextView) findViewById(R.id.module_one);
            module_two = (TextView) findViewById(R.id.module_two);

        // 원하는 각도를 정한 후 완료 버튼을 눌렀을 경우
            module_one.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        ProjectActivity.adapter.add_item(ProjectActivity.select_layout, angle.getProgress() + " 도 (1) ");
                    } catch (Exception e) {
                        Adjust_ProjectActivity.adapter.add_item(Adjust_ProjectActivity.select_layout, angle.getProgress() + " 도 (1) ");
                    }
                    finish();
                }
            });
        module_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    value_save.angle = angle.getProgress();
                    ProjectActivity.adapter.add_item(ProjectActivity.select_layout, angle.getProgress() + " 도 (2) ");
                } catch (Exception e) {
                    value_save.angle = angle.getProgress();
                    Adjust_ProjectActivity.adapter.add_item(Adjust_ProjectActivity.select_layout, angle.getProgress() + " 도 (2) ");
                }
                finish();
            }
        });

    }

        // 사용자가 뒤로가기 버튼 클릭시 아무런 행동을 하지 않음
        @Override
        public void onBackPressed () {

        }
}
