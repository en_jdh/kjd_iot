package kjd.tool;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import kjd.main_acitivity.Adjust_ProjectActivity;
import kjd.main_acitivity.ProjectActivity;
import kjd.iot_researcher.R;

// Text 아이템을 선택했을 경우 Text를 입력하기 위해 팝업으로 뜨는 Activity
public class Text_Activity extends Activity{
    EditText text;
    TextView ok;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount = 0.7f;
        getWindow().setAttributes(layoutParams);
        setContentView(R.layout.text_write_layout);

        this.setFinishOnTouchOutside(false);
        text = (EditText) findViewById(R.id.edittext);
        ok = (TextView) findViewById(R.id.ok);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        text.requestFocus();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(text.getWindowToken(), 0);
                try {
                    ProjectActivity.adapter.add_item(ProjectActivity.select_layout, text.getText().toString());
                }
                catch (Exception e){
                    Adjust_ProjectActivity.adapter.add_item(Adjust_ProjectActivity.select_layout, text.getText().toString());
                }
                finish();
            }
        });
    }

    // 사용자가 뒤로가기 버튼 클릭시 아무런 행동을 하지 않음
    @Override
    public void onBackPressed() {

    }
}
