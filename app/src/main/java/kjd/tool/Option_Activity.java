package kjd.tool;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import kjd.main_acitivity.ProjectActivity;
import kjd.iot_researcher.R;
import kjd.iot_researcher.value_save;


// Angle 아이템을 선택했을 경우 각도를 입력하기 위해 팝업으로 뜨는 Activity
public class Option_Activity extends Activity{
    TextView cancle, submit;
    RadioGroup pn, grow, state;
    EditText doping_text, thick_text, oxide_text;
    String pn_text,grow_text,state_text;
    @Override
    public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            layoutParams.dimAmount = 0.7f;
            getWindow().setAttributes(layoutParams);
            setContentView(R.layout.option_layout);

            this.setFinishOnTouchOutside(false);
            submit = (TextView) findViewById(R.id.submit_text);
            cancle = (TextView) findViewById(R.id.cancle_text);
            pn = (RadioGroup) findViewById(R.id.group_pn);
            grow = (RadioGroup) findViewById(R.id.group_grow);
            state = (RadioGroup) findViewById(R.id.group_state);
            doping_text = (EditText) findViewById(R.id.doping_text);
            thick_text = (EditText) findViewById(R.id.thick_text);
            oxide_text = (EditText) findViewById(R.id.oxide_text);

        // 확인, 취소를 눌렀을 경우.
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pn.getCheckedRadioButtonId() == R.id.p_button)  pn_text = "p";
                else pn_text = "n";

                if(grow.getCheckedRadioButtonId() == R.id.ozz_button) grow_text = "100";
                else if(grow.getCheckedRadioButtonId() == R.id.ooz_button) grow_text = "110";
                else grow_text = "111";

                if(state.getCheckedRadioButtonId() == R.id.uni_button) state_text = "Uniform";
                else if(state.getCheckedRadioButtonId() == R.id.gaus_button) state_text ="Gaussian S-D";
                else state_text = "Gaussian S-D HALO";

                value_save.pn = pn_text;
                value_save.grow = grow_text;
                value_save.state = state_text;
                value_save.doping = doping_text.getText().toString();
                value_save.thick = thick_text.getText().toString();
                value_save.oxide = oxide_text.getText().toString();

                finish();
                startActivity(new Intent(Option_Activity.this,ProjectActivity.class));
            }
        });

        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

        // 사용자가 뒤로가기 버튼 클릭시 아무런 행동을 하지 않음
        @Override
        public void onBackPressed () {

        }
}
