package kjd.tool;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import kjd.main_acitivity.Adjust_ProjectActivity;
import kjd.main_acitivity.ProjectActivity;
import kjd.iot_researcher.R;


// Delay 아이템을 사용했을 경우 팝업창으로 뜨는 Activity
public class Delay_Activity extends Activity{
    DiscreteSeekBar min, second;
    TextView ok;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        layoutParams.dimAmount = 0.7f;
        getWindow().setAttributes(layoutParams);
        setContentView(R.layout.delay_time_layout);

        this.setFinishOnTouchOutside(false);
        min = (DiscreteSeekBar) findViewById(R.id.min);
        second = (DiscreteSeekBar) findViewById(R.id.second);
        ok = (TextView) findViewById(R.id.ok);

        // 원하는 분과 초를 정한 후 완료 버튼을 눌렀을 경우
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ProjectActivity.adapter.add_item(ProjectActivity.select_layout, min.getProgress() + " 분 " + second.getProgress() + " 초");
                }
                catch (Exception e){
                    Adjust_ProjectActivity.adapter.add_item(Adjust_ProjectActivity.select_layout, min.getProgress() + " 분 "+ second.getProgress() + " 초");
                }
                finish();
            }
        });
    }

        // 사용자가 뒤로가기 버튼 클릭시 아무런 행동을 하지 않음
    @Override
    public void onBackPressed() {

    }
}
