package kjd.main_acitivity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import kjd.iot_researcher.Load_Anal_ProjectActivity;
import kjd.iot_researcher.Load_ProjectActivity;
import kjd.iot_researcher.MyFirebaseInstanceIDService;
import kjd.iot_researcher.R;
import kjd.iot_researcher.value_save;

// IntroActivty가 실행된 후 Main이 실행된다.
public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {
    TextView new_project;
    TextView load_project;
    TextView analyze_project;
    TextView on_off_text;
    ImageView on_off_image;
    static String check_on_off;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        putData();
        tostart();
        if(value_save.check_on_off.equals("off")){
            on_off_image.setImageResource(R.drawable.red_circle);
            on_off_text.setText("Server Off");
        }
    }

    // 각 메뉴가 클릭이 됐을 경우 판단
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.new_project:
                if(value_save.check_on_off.equals("off")){
                    Toast.makeText(MainActivity.this, "Server off", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this, Choose_ProjectActivity.class));
                }else {
               //     startActivity(new Intent(MainActivity.this, ProjectActivity.class));
                    startActivity(new Intent(MainActivity.this, Choose_ProjectActivity.class));
                }
                break;
            case R.id.load_project:
                if(value_save.check_on_off.equals("off")){
                    Toast.makeText(MainActivity.this, "Server off", Toast.LENGTH_SHORT).show();
                }else {
                    startActivity(new Intent(MainActivity.this, Load_ProjectActivity.class));
                }
                break;
            case R.id.analyze_project:
                if(value_save.check_on_off.equals("off")){
                    Toast.makeText(MainActivity.this, "Server off", Toast.LENGTH_SHORT).show();
                }else {
                    startActivity(new Intent(MainActivity.this, Load_Anal_ProjectActivity.class));
                }
                break;
        }
    }

    // 각 메뉴가 터치가 됐을 경우 버튼의 색상 변경을 위한 판단
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()){
            case R.id.new_project:
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    new_project.setBackground(getDrawable(R.drawable.round_clicked));
                }
                else if(event.getAction() == MotionEvent.ACTION_UP){
                    new_project.setBackground(getDrawable(R.drawable.round));
                }
                break;
            case R.id.load_project:
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    load_project.setBackground(getDrawable(R.drawable.round_clicked));
                }
                else if(event.getAction() == MotionEvent.ACTION_UP){
                    load_project.setBackground(getDrawable(R.drawable.round));
                }
                break;
            case R.id.analyze_project:
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    analyze_project.setBackground(getDrawable(R.drawable.round_clicked));
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    analyze_project.setBackground(getDrawable(R.drawable.round));
                }
                break;
        }
        return false;
    }

    // view들의 초기화와 리스너 등록
    public void tostart(){
        // 초기화
        new_project = (TextView) findViewById(R.id.new_project);
        load_project = (TextView) findViewById(R.id.load_project);
        analyze_project = (TextView) findViewById(R.id.analyze_project);
        on_off_text = (TextView) findViewById(R.id.on_off_text);
        on_off_image = (ImageView) findViewById(R.id.on_off_image);

        // 터치 리스너 등록
        new_project.setOnClickListener(this);
        new_project.setOnTouchListener(this);
        load_project.setOnClickListener(this);
        load_project.setOnTouchListener(this);
        analyze_project.setOnClickListener(this);
        analyze_project.setOnTouchListener(this);
    }

    public void putData(){

        class push_data extends AsyncTask<Void, Void, String> {
            @Override
            protected String doInBackground(Void... params) {
                try {
                    new MyFirebaseInstanceIDService().onTokenRefresh();
                    return "";
                }catch(Exception e){
                    return null;
                }
            }
            @Override
            protected void onPostExecute(String result){

            }
        }
        push_data g = new push_data();
        g.execute();
    }
}
