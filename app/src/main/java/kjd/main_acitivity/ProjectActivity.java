package kjd.main_acitivity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import kjd.iot_researcher.CustomRecyclerView;
import kjd.iot_researcher.ExecuteActivity;
import kjd.iot_researcher.R;
import kjd.iot_researcher.value_save;
import kjd.item.DividerItemDecoration;
import kjd.item.Item;
import kjd.item.ItemAdapter;
import kjd.item.ItemLayoutManger;
import kjd.tool.Angle_Activity;
import kjd.tool.Delay_Activity;
import kjd.tool.Detect_Activity;
import kjd.tool.Text_Activity;

// New Project를 선택시 들어오는 Project를 만들 수 있는 Activity
public class ProjectActivity extends AppCompatActivity implements View.OnClickListener,View.OnTouchListener, View.OnDragListener{
    ImageView edit_title;
    EditText project_name;
    TextView project_name_text;
    TextView date;
    public static String select_layout;
    RelativeLayout in_drag;
    View.DragShadowBuilder shadow; // drag 용 그림자
    ImageView push, spin, detect, temp, text;
    TextView next;
    TextView save_execute;
    CustomRecyclerView recyclerView;
    WebView webView, webView2, webView3;
    int count, count_execute;
    String save = "http://kjdiot.iptime.org/dong/dong_put_ex.php?";
    String save_value = "http://kjdiot.iptime.org/dong/dong_put_value_ex.php?";
    public static ItemAdapter adapter;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_project);
        tostart();
        // 현재 날짜 표시
        date.setText(new SimpleDateFormat("yyyy-MM-dd-E요일").format(Calendar.getInstance().getTime()));
        // recyclerView의 adapter와 layout, anim 적용
        adapter = new ItemAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this));
        recyclerView.setLayoutManager(new ItemLayoutManger(this));
        adapter.add_item("temp","");
        for(int i = 0; i<6;i++){
            value_save.execute[i]="";
        }
    }

    // 각 아이템 클릭이 됐을 경우 판단
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_title:
                if (project_name_text.getVisibility() == View.VISIBLE) {
                    Toast toast = Toast.makeText(this, "수정후 체크 버튼을 눌러주세요", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    edit_title.setImageDrawable(getDrawable(R.drawable.success));
                    project_name.setVisibility(View.VISIBLE);
                    project_name_text.setVisibility(View.GONE);
                    // 수정 이미지 클릭시 키보드 나타나게 하기 & editText에 커서주기
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    project_name.requestFocus();
                    // 이름을 잘 못 했을경우 다시 edit을 할 때, 바로 수정할 수 있도록 커서를 끝으로 보냄
                    project_name.setSelection(project_name.length());
                } else {
                    // 이름을 쓰지 않았을 경우
                    if (project_name.getText().toString().equals("")) {
                        Toast toast = Toast.makeText(this, "빈 이름 입니다", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                    // 이름을 썻을 경우
                    else {
                        Toast.makeText(ProjectActivity.this, "수정 완료", Toast.LENGTH_SHORT).show();
                        // 완료 이미지 클릭시 키보드 감추기
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(project_name.getWindowToken(), 0);
                        // 한글 밑줄 생성된것을 없애기 위해 null 값을 붙여주었다
                        project_name.setText(project_name.getText().toString());
                        edit_title.setImageDrawable(getDrawable(R.drawable.edit));
                        project_name_text.setText(project_name.getText());
                        project_name.setVisibility(View.GONE);
                        project_name_text.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case R.id.save_list:
                String name = project_name_text.getText().toString();
                name = name.replaceAll(" ","_");
                String item = "";
                String value = "";
                save = save +"name="+ name;
                save_value = save_value +"name="+name;
                String num = "";
                //save();
                for(int i = 0; i < adapter.visibleItems.size(); i++){
                    if(adapter.visibleItems.get(i).getViewType() == 0 ){
                        if(adapter.visibleItems.get(i).getName().equals("PUSH"))
                            adapter.expandChildItems(i);
                        else if(adapter.visibleItems.get(i).getName().equals("SPIN"))
                            adapter.expandChildItems(i);
                        else if(adapter.visibleItems.get(i).getName().equals("DETECT"))
                            adapter.expandChildItems(i);
                        else if(adapter.visibleItems.get(i).getName().equals("TEMPERATURE"))
                            adapter.expandChildItems(i);
                        else if(adapter.visibleItems.get(i).getName().equals("TEXT"))
                            adapter.expandChildItems(i);
                        else if(adapter.visibleItems.get(i).getName().equals("DELAY"))
                            adapter.expandChildItems(i);
                    }
                }
                for(int i = 0, set = 1; i < adapter.visibleItems.size(); i++){
                    if(set == 1) num = "&one=";
                    else if(set == 2) num = "&two=";
                    else if(set == 3) num = "&three=";
                    else if(set == 4) num = "&four=";
                    else if(set == 5) num = "&five=";
                    else if(set == 6) num = "&six=";
                    else if(set == 7) num = "&seven=";
                    else if(set == 8) num = "&eight=";
                    if(adapter.visibleItems.get(i).getViewType() == 0){
                        if(adapter.visibleItems.get(i).getName().equals("PUSH")) {
                            item = "push";
                            save = save +num +item;
                                value_save.execute[count_execute++] = "21";
                                Log.d("111111",(count_execute)+"번째 실행 ::::: "+value_save.execute[count_execute-1]);
                        }
                        else if(adapter.visibleItems.get(i).getName().equals("SPIN")) {
                            item = "spin";
                            save = save +num +item;
                        }
                        else if(adapter.visibleItems.get(i).getName().equals("DETECT")) {
                            item = "detect";
                            save = save +num +item;
                        }
                        else if(adapter.visibleItems.get(i).getName().equals("TEMPERATURE")) {
                            item = "temp";
                            save = save +num +item;
                        }
                        else if(adapter.visibleItems.get(i).getName().equals("DELAY")) {
                            item = "delay";
                            save = save +num +item;
                        }
                        else{
                            set--;
                        }
                        set++;
//                        else if(adapter.visibleItems.get(i).getName().equals("TEXT"))
//                            item = "text";
                    }
                    if(adapter.visibleItems.get(i).getViewType() == 1){
                        if(set == 2) num = "&one=";
                        else if(set == 3) num = "&two=";
                        else if(set == 4) num = "&three=";
                        else if(set == 5) num = "&four=";
                        else if(set == 6) num = "&five=";
                        else if(set == 7) num = "&six=";
                        else if(set == 8) num = "&seven=";
                        else if(set == 9) num = "&eight=";
                        if(item.equals("spin")) {
                            value = adapter.visibleItems.get(i).getName();
                            value = value.replace("각도 : ","spin_value");
                            save_value = save_value + num + value;
                            if(value.contains("(1)")){
                                value = value.replace("spin_value","11");
                                value = value.replace(" 도 (1)","");
                            }else{
                                value = value.replace("spin_value","12");
                                value = value.replace(" 도 (2)","");
                            }
                            value_save.execute[count_execute++] = value;
                            Log.d("111111",(count_execute)+"번째 실행 ::::: "+value_save.execute[count_execute-1]);
                        }
                        else if(item.equals("detect")) {
                            value = adapter.visibleItems.get(i).getName();
                            value = value.replace("촬영 횟수: ","detect_value");
                            save_value = save_value + num + value;
                        }
                        else if(item.equals("delay")) {
                            value = adapter.visibleItems.get(i).getName();
                            value = value.replace("지연 시간 : ","");
                            value = value.replace(" ","");
                            value = value.replace("분",".");
                            value = value.replace("초","");
                            save_value = save_value + num + value;
                        }
                    }

                }
                if(adapter.visibleItems.size() == 0){
                    Toast.makeText(ProjectActivity.this, "Empty Project", Toast.LENGTH_SHORT).show();
                }
                // 이름이 수정중일 경우 이름을 완성 요청
                else if(project_name_text.getVisibility() == View.GONE){
                    Toast.makeText(ProjectActivity.this, "이름을 확인 해주세요", Toast.LENGTH_SHORT).show();
                }
                // 그 이외 저장 확인 요청
                else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("저장 및 실행 확인")
                            .setCancelable(false)
                            .setMessage("Project를 실행하시겠습니까?")
                            .setNegativeButton("저장만", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    save();
                                    adapter.visibleItems.clear();
                                    finish();
                                }
                            })
                            .setPositiveButton("저장 및 실행", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    save();
                                    startActivity(new Intent(ProjectActivity.this,ExecuteActivity.class));
                                    Toast.makeText(ProjectActivity.this, "진행사항을 확인해 주세요.", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();

                }
//                finish();

//                webView3.loadUrl("http://117.17.184.183/js/angle.php?angle='21'");
                //webView3.loadUrl("http://117.17.184.183/js/angle.php?angle='11"+ kjd.iot_researcher.value_save.angle1+"'");
                //webView3.loadUrl("http://117.17.184.183/js/angle.php?angle='12"+ kjd.iot_researcher.value_save.angle1+"'");
//                try {
//                    URL url = new URL(save);
//                    URLConnection conn = url.openConnection();
//                    conn.setUseCaches(false);
//                    conn.getInputStream();
//                    Log.d("111111",save);
//                }
//                catch (Exception e){
//                    Log.d("111111",save);
//                    Log.d("111111","error");
//                }

                break;
        }
    }


    // view들의 초기화와 리스너 등록
    public void tostart() {
        project_name = (EditText) findViewById(R.id.project_name);
        project_name_text = (TextView) findViewById(R.id.project_name_text);
        date = (TextView) findViewById(R.id.date);
        edit_title = (ImageView) findViewById(R.id.edit_title);
        edit_title.setOnClickListener(this);
        push = (ImageView) findViewById(R.id.push);
        spin = (ImageView) findViewById(R.id.spin);
        detect = (ImageView) findViewById(R.id.detect);
        temp = (ImageView) findViewById(R.id.temp);
        text = (ImageView) findViewById(R.id.text);
        next = (TextView) findViewById(R.id.next);
        save_execute = (TextView) findViewById(R.id.save_list);
        webView = (WebView) findViewById(R.id.webview);
        webView2 = (WebView) findViewById(R.id.webview2);
        webView3 = (WebView) findViewById(R.id.webview3);
        save_execute.setOnClickListener(this);
        push.setOnTouchListener(this);
        spin.setOnTouchListener(this);
        detect.setOnTouchListener(this);
        temp.setOnTouchListener(this);
        text.setOnTouchListener(this);
        next.setOnTouchListener(this);
        in_drag = (RelativeLayout) findViewById(R.id.example);
        in_drag.setOnDragListener(this);
        recyclerView = (CustomRecyclerView) findViewById(R.id.recyclerview);
    }


    // drag 했을 경우 event에 따른 판단
    @Override
    public boolean onDrag(View v, DragEvent event) {
        final int action = event.getAction();
        switch (action){
            // drag 했다가 놓았을 경우
            case DragEvent.ACTION_DROP:
                if(v == findViewById(R.id.example)) {
                    if(select_layout.equals("delay")){
                        startActivity(new Intent(this,Delay_Activity.class));
                    }
                    else if(select_layout.equals("text")){
                        startActivity(new Intent(this,Text_Activity.class));
                    }
                    else if(select_layout.equals("spin")){
                        startActivity(new Intent(this,Angle_Activity.class));
                    }
                    else if(select_layout.equals("detect")){
                        startActivity(new Intent(this,Detect_Activity.class));
                    }
                    else {
                        adapter.add_item(select_layout,"");
                    }
                }
                else{
                    Toast.makeText(ProjectActivity.this, "정확히 넣어주세요.", Toast.LENGTH_SHORT).show();
                }
                break;
        }

        return true;
    }

    // 아이템을 터치 했을 경우 판단
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        shadow = new View.DragShadowBuilder(v);
        v.startDrag(null,shadow,v,0);
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            if (v.getId() == R.id.push) {
                select_layout = "push";
            } else if (v.getId() == R.id.spin) {
                select_layout = "spin";
            } else if (v.getId() == R.id.detect) {
                select_layout = "detect";
            } else if (v.getId() == R.id.temp) {
                select_layout = "temp";
            } else if (v.getId() == R.id.text) {
                select_layout = "text";
            } else {
                select_layout = "delay";
            }
        }
        return true;
    }

    // 뒤로가기 키를 눌렸을 경우
    @Override
    public void onBackPressed() {
        // 아이템을 추가 하지 않았으면 바로 종료
        if(adapter.visibleItems.size() == 0){
           finish();
        }
        // 이름이 수정중일 경우 이름을 완성 요청
        else if(project_name_text.getVisibility() == View.GONE){
            Toast.makeText(ProjectActivity.this, "이름을 확인 해주세요", Toast.LENGTH_SHORT).show();
        }
        // 그 이외 저장 확인 요청
        else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("저장 확인")
                    .setMessage("작성 중인 Project가 있습니다.\n저장하시겠습니까?")
                    .setNegativeButton("아니요", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            adapter.visibleItems.clear();
                            value_save.pn = "";
                            value_save.grow = "";
                            value_save.state = "";
                            value_save.doping = "";
                            value_save.thick = "";
                            value_save.oxide = "";
                            finish();

                        }
                    })
                    .setPositiveButton("저장", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            save();
                            finish();
                            adapter.visibleItems.clear();
                            Toast.makeText(ProjectActivity.this, "저장되었습니다.", Toast.LENGTH_SHORT).show();
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();

        }
    }
    // 작성 Project 저장
    public void save(){
        ArrayList<Item> save_item = new ArrayList<>();
        save_item = adapter.visibleItems;

        SharedPreferences count_data = getSharedPreferences("count",MODE_PRIVATE);
        SharedPreferences.Editor editor = count_data.edit();
        if(count_data.getInt("count",0) == 0){
            editor.putInt("count",1);
            editor.commit();
        }
        else {
            editor.putInt("count",count_data.getInt("count",0)+1);
            editor.commit();
        }
        count = count_data.getInt("count",0);
        save = save+"&num="+count;
        save_value = save_value+"&num="+count;
        webView.loadUrl(save+"&num="+count);
        webView2.loadUrl("http://kjdiot.iptime.org/dong/setting.php?"+"&num="+count+"&type="
                +value_save.pn+"&profile="+value_save.state+"&direction="+value_save.grow+"&concentrate="+value_save.doping+"&thickness="+value_save.thick+"&oxide="+value_save.oxide);
        Log.d("111111","count before :::: "+count+"");
        Log.d("111111","save_value ::: "+save_value);
        Log.d("111111","save :::"+save);

        ArrayList<String> save_text = new ArrayList<>();
        save_text.add(project_name_text.getText().toString());
        save_text.add(date.getText().toString());

            // name과 date를 담은 String Array를 Object로 저장
            try {
                ObjectOutputStream os = new ObjectOutputStream(
                        new BufferedOutputStream(new FileOutputStream(
                                getFilesDir().getAbsolutePath() + "/name_data"+count+".ser"
                        )));
                os.writeObject(save_text);
                os.close();
            }
            catch (Exception e){
                Toast.makeText(ProjectActivity.this, "저장 오류 발생", Toast.LENGTH_SHORT).show();
                finish();
            }
        try {
            ObjectOutputStream os = new ObjectOutputStream(
                    new BufferedOutputStream(
                            new FileOutputStream(
                            getFilesDir().getAbsolutePath() + "/inner_data"+count+".ser"
                    )));
            Log.d("111111","생성 :::: "+count);
            os.writeObject(save_item);
            os.close();
        }
        catch (Exception e){
            Toast.makeText(ProjectActivity.this, "저장 오류 발생", Toast.LENGTH_SHORT).show();
            finish();
        }
        }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.visibleItems.clear();
    }
}
