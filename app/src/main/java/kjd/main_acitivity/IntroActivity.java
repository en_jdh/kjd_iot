package kjd.main_acitivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import kjd.iot_researcher.R;
import kjd.iot_researcher.value_save;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

// 앱을 처음 실행 시켰을경우 실행되는 Activity
public class IntroActivity extends AppCompatActivity {
    Handler intro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        // 전환을 위한 handler생성
        intro = new Handler();
        // server 확인
        putData("http://kjdiot.iptime.org/");
    }

    // 인트로 화면에서 메인 화면으로의 전환
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(IntroActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
            // 자연스럽게 넘어가기 위한 애니메이션
            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
        }
    };

    // 인트로화면에서 back 키를 눌렀을 경우 메인화면으로 넘어가지 않기 위해서
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intro.removeCallbacks(runnable);
    }

    // Check server on off
    public void putData(final String url){

        // AsyncTask를 통해 Thread 처리
        class GetDataJSON extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                // OkHttp를 통한 통신
                OkHttpClient client = new OkHttpClient.Builder()
                        .connectTimeout(1, TimeUnit.SECONDS)
                        .build();
                //request
                Request request = new Request.Builder()
                        .url(url)
                        .build();

                client.newCall(request).enqueue(callback);


                return null;
            }

        }
        GetDataJSON g = new GetDataJSON();
        g.execute(url);
    }
    private Callback callback = new Callback() {
        @Override
        public void onFailure(Call call, IOException e) {
            value_save.check_on_off = "off";
            // Runnable을 2초 뒤 실행
            intro.postDelayed(runnable,2000);
        }

        @Override
        public void onResponse(Call call, Response response) throws IOException {
            value_save.check_on_off = "on";
            // Runnable을 2초 뒤 실행
            intro.postDelayed(runnable,2000);
        }
    };

}
