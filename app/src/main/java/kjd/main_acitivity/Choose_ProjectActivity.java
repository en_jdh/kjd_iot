package kjd.main_acitivity;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import kjd.adapter.ChoosePagerAdapter;
import kjd.iot_researcher.R;

// New Project를 선택시 들어오는 Project를 만들 수 있는 Activity
public class Choose_ProjectActivity extends AppCompatActivity {
    TextView textView, summary;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_project);

        textView = (TextView) findViewById(R.id.choose_text);
        summary = (TextView) findViewById(R.id.summary);

        ViewPager viewPager = (ViewPager) findViewById(R.id.choose_viewpager);
        PagerAdapter adapter = new ChoosePagerAdapter(getSupportFragmentManager(), 3);
        viewPager.setAdapter(adapter);

        // ViewPager가 스크롤 될경우 해당 position에 맞는 Text로 바꿔준다.
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        textView.setText("Free Experiment");
                        summary.setText("조건없이 자유롭게 Project를 만들 수 있습니다.");
                        break;
                    case 1:
                        textView.setText("MOSFET");
                        summary.setText("MOSFET용 실험환경을 설정 할 수 있습니다.");
                        break;
                    case 2:
                        textView.setText("BJT");
                        summary.setText("BJT용 실험환경을 설정 할 수 있습니다.");
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
